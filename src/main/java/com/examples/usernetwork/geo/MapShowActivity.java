
package com.examples.usernetwork.geo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.geo.BackendlessGeoQuery;
import com.backendless.geo.GeoPoint;
import com.backendless.geo.Units;
import com.examples.usernetwork.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class MapShowActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
                                                         LocationListener, ActivityCompat.OnRequestPermissionsResultCallback
{
  private TextView categoryTitle;
  private Button showAsTextButton, filterButton;

  public static String whereClause = "";
  public static String category = "";
  public static GoogleApiClient mGoogleApiClient;
  public static boolean searchInRadius = false;
  public static double radius;
  public static Units units;

  @Override
  public void onCreate( Bundle savedInstanceState )
  {
    super.onCreate( savedInstanceState );
    setContentView( R.layout.map_show );

    if( category.isEmpty() )
    {
      category = getIntent().getStringExtra( "category" );
    }

    if( mGoogleApiClient == null )
    {
      mGoogleApiClient = new GoogleApiClient.Builder( this )
              .addApi( LocationServices.API )
              .addConnectionCallbacks( this )
              .addOnConnectionFailedListener( this )
              .build();
      mGoogleApiClient.connect();
    }

    List<String> chosenCategories = new ArrayList<String>();
    chosenCategories.add( category );
    final BackendlessGeoQuery backendlessGeoQuery = new BackendlessGeoQuery();
    backendlessGeoQuery.setCategories( chosenCategories );
    if( !whereClause.isEmpty() )
    {
      backendlessGeoQuery.setWhereClause( whereClause );
    }
    if( searchInRadius )
    {
      if( mGoogleApiClient.isConnected() )
      {
        Location mCurrentLocation = MapShowActivity.getLastLocation(this);
        backendlessGeoQuery.setLatitude( mCurrentLocation.getLatitude() );
        backendlessGeoQuery.setLongitude( mCurrentLocation.getLongitude() );
        backendlessGeoQuery.setRadius( radius );
        backendlessGeoQuery.setUnits( units );
      }
      else
      {
        Toast.makeText( this, "Current location not available", Toast.LENGTH_SHORT ).show();
      }
    }

    initUI();

    Backendless.Geo.getPoints( backendlessGeoQuery, new DefaultCallback<List<GeoPoint>>( this )
    {
      @Override
      public void handleResponse( List<GeoPoint> response )
      {
        final List<GeoPoint> geoPoints = response;
        ((MapFragment) getFragmentManager().findFragmentById( R.id.map )).getMapAsync( new OnMapReadyCallback()
        {
          @Override
          public void onMapReady( GoogleMap googleMap )
          {
            googleMap.setMyLocationEnabled( true );

            for( GeoPoint geoPoint : geoPoints )
            {
              googleMap.addMarker( new MarkerOptions().position( new LatLng( geoPoint.getLatitude(), geoPoint.getLongitude() ) ).title( "ObjectId: " + geoPoint.getObjectId() ) );
            }
          }
        } );

        super.handleResponse( response );
      }
    } );
  }

  public static Location getLastLocation(Context ctx)
  {
    return LocationServices.FusedLocationApi.getLastLocation( mGoogleApiClient );
  }

  private void initUI()
  {
    categoryTitle = (TextView) findViewById( R.id.categoryTitle );
    showAsTextButton = (Button) findViewById( R.id.showAsTextButton );
    filterButton = (Button) findViewById( R.id.filterButton );

    String title = String.format( getResources().getString( R.string.browsing_category_page_title ), MapShowActivity.category );
    categoryTitle.setText( title );

    showAsTextButton.setOnClickListener( new View.OnClickListener()
    {
      @Override
      public void onClick( View v )
      {
        startActivity( new Intent( MapShowActivity.this, ShowGeoPointsTextActivity.class ) );
      }
    } );

    filterButton.setOnClickListener( new View.OnClickListener()
    {
      @Override
      public void onClick( View v )
      {
        startActivity( new Intent( MapShowActivity.this, FilterActivity.class ) );
      }
    } );
  }

  @Override
  public void onConnected( Bundle bundle )
  {
    LocationRequest mLocationRequest = new LocationRequest();
    LocationServices.FusedLocationApi.requestLocationUpdates( mGoogleApiClient, mLocationRequest, this );
  }

  @Override
  public void onConnectionSuspended( int i )
  {

  }

  @Override
  public void onConnectionFailed( ConnectionResult connectionResult )
  {

  }

  @Override
  public void onLocationChanged( Location location )
  {

  }
}
                        