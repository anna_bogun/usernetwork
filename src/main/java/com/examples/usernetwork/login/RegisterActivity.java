package com.examples.usernetwork.login;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.examples.usernetwork.R;

public class RegisterActivity extends Activity {
    private final static java.text.SimpleDateFormat SIMPLE_DATE_FORMAT = new java.text.SimpleDateFormat("yyyy/MM/dd");
    private static final Integer MINIMAL_AGE = 5;

    private EditText nameField;
    private EditText emailField;
    private EditText passwordField;
    private EditText countryField;
    private EditText ageField;
    private Button registerButton;

    private String name;
    private String email;
    private String password;
    private String country;
    private Integer age;
    private String sex;

    private BackendlessUser user;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initUI();
    }

    private void initUI() {
        nameField = findViewById(R.id.nameField);
        emailField = findViewById(R.id.emailField);
        countryField = findViewById(R.id.countryField);
        ageField = findViewById(R.id.ageField);

        passwordField = findViewById(R.id.passwordField);
        registerButton = findViewById(R.id.registerButton);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRegisterButtonClicked();
            }
        });
    }

    private void onRegisterButtonClicked() {
        String nameText = nameField.getText().toString().trim();
        String emailText = emailField.getText().toString().trim();
        String passwordText = passwordField.getText().toString().trim();
        String countryText = countryField.getText().toString().trim();
        String ageText = ageField.getText().toString().trim();

        if (emailText.isEmpty()) {
            Toast.makeText(this, "Field 'email' cannot be empty.", Toast.LENGTH_SHORT).show();
            return;
        } else
            email = emailText;

        if (passwordText.isEmpty()) {
            Toast.makeText(this, "Field 'password' cannot be empty.", Toast.LENGTH_SHORT).show();
            return;
        } else
            password = passwordText;

        if (nameText.isEmpty()) {
            Toast.makeText(this, "Field 'name' cannot be empty.", Toast.LENGTH_SHORT).show();
            return;
        } else
            name = nameText;

        if (countryText.isEmpty()) {
            Toast.makeText(this, "Field 'country' cannot be empty.", Toast.LENGTH_SHORT).show();
            return;
        } else
            country = countryText;

        if (ageText.isEmpty()) {
            Toast.makeText(this, "Field 'age' cannot be empty.", Toast.LENGTH_SHORT).show();
            return;
        } else {
            age = Integer.parseInt(ageText);
            if (age <= MINIMAL_AGE) {
                Toast.makeText(this, "Minimal age is " + MINIMAL_AGE + "! Registration is not allowed", Toast.LENGTH_SHORT).show();
                age = null;
                return;
            }
        }


        final BackendlessUser user = new BackendlessUser();

        if (email != null) {
            user.setEmail(email);
        }

        if (password != null) {
            user.setPassword(password);
        }

        if (name != null) {
            user.setProperty("name", name);
        }

        if (country != null) {
            user.setProperty("country", country);
        }
        if (age != null) {
            user.setProperty("age", age);
        }
        user.setProperty("sex", sex);


        Backendless.UserService.register(user, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {
                Resources resources = getResources();
                String message = String.format(resources.getString(R.string.registration_success_message), resources.getString(R.string.app_name));

                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                builder.setMessage(message).setTitle(R.string.registration_success);

                //TODO
                //                File directory = new File(user.getEmail());
                //                directory.mkdir();
//                Backendless.Files.moveFile(directory.getName(), Defaults.DEFAULT_PATH_ROOT + "/" + directory.getName());
                AlertDialog dialog = builder.create();
                dialog.show();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                builder.setMessage(fault.getMessage()).setTitle(R.string.registration_error);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.male:
                if (checked)
                    sex = "male";
                break;
            case R.id.female:
                if (checked)
                    sex = "female";
                break;
        }
    }
}

