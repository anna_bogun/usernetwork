package com.examples.usernetwork.login;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.examples.usernetwork.R;
import com.examples.usernetwork.file.BrowseActivity;
import com.examples.usernetwork.file.Defaults;
import com.examples.usernetwork.file.FileUpload;
import com.examples.usernetwork.file.PermissionRequestor;
import com.examples.usernetwork.file.UploadingActivity;
import com.examples.usernetwork.geo.GeoCategoriesListActivity;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class LoginResult extends Activity {
    static final String userInfo_key = "BackendlessUserInfo";
    static final String logoutButtonState_key = "LogoutButtonState";
    private Button bkndlsLogoutButton;

    private TextView welcomeTextField;
    private TextView urlField;
    private Button takePhotoButton;
    private Button uploadFileButton;
    private Button changeAvatar;
    private Button geoButton;
    private ImageView avatarImage;
    private FileUpload fileUpload = new FileUpload();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        if (Defaults.APPLICATION_ID.equals("") || Defaults.API_KEY.equals("")) {
            showAlert(this, "Missing application ID and secret key arguments. Login to Backendless Console, select your app and get the ID and key from the Manage - App Settings screen. Copy/paste the values into the Backendless.initApp call");
            return;
        }
        Backendless.setUrl(Defaults.SERVER_URL);
        Backendless.initApp(this, Defaults.APPLICATION_ID, Defaults.API_KEY);
        initLogoutButton();
        welcomeTextField = findViewById(R.id.welcomeTextField);
        urlField = findViewById(R.id.urlField);
        takePhotoButton = findViewById(R.id.takePhotoButton);
        uploadFileButton = findViewById(R.id.uploadFileButton);
        changeAvatar = findViewById(R.id.changeAvatar);
        avatarImage = findViewById(R.id.avatarImage);
        geoButton = findViewById(R.id.geoButton);
        takePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, Defaults.CAMERA_REQUEST);
            }
        });
        Object avatar = Backendless.UserService.CurrentUser().getProperty("avatar");

        if (avatar != null && avatar != "") {
            setAvatar(avatar.toString());
        }

        uploadFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                try {
                    startActivityForResult(intent, Defaults.UPLOAD_FILE_REQUEST);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(LoginResult.this, "There are no file explorer clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        changeAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");

                try {
                    startActivityForResult(intent, Defaults.CHANGE_AVATAR);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(LoginResult.this, "There are no file explorer clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        geoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginResult.this, GeoCategoriesListActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.browseUploadedButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginResult.this, BrowseActivity.class);
                startActivity(intent);
            }
        });

        System.out.println("Current user is " + Backendless.UserService.CurrentUser());
        welcomeTextField.setText("Hi, " + Backendless.UserService.CurrentUser().getProperty("name") + " from " +
                Backendless.UserService.CurrentUser().getProperty("country") +
                "!");

        PermissionRequestor.checkPermissionREAD_EXTERNAL_STORAGE(this);
    }

    private void initLogoutButton() {
        bkndlsLogoutButton = findViewById(R.id.button_bkndlsBackendlessLogout);
        Intent intent = getIntent();
        System.out.println(bkndlsLogoutButton);
        boolean logoutButtonState = intent.getBooleanExtra(logoutButtonState_key, true);

        if (logoutButtonState) {
            bkndlsLogoutButton.setVisibility(View.VISIBLE);

        } else {
            bkndlsLogoutButton.setVisibility(View.INVISIBLE);

        }
        initUIBehaviour();
    }

    private void initUIBehaviour() {
        bkndlsLogoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutFromBackendless();
            }
        });
    }

    private void logoutFromBackendless() {
        Backendless.UserService.logout(new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void response) {
                bkndlsLogoutButton.setVisibility(View.INVISIBLE);
                finish();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                //todo
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;

        switch (requestCode) {
            case Defaults.CAMERA_REQUEST:
                data.setClass(getBaseContext(), UploadingActivity.class);
                startActivityForResult(data, Defaults.URL_REQUEST);
                break;
            case Defaults.UPLOAD_FILE_REQUEST:
                fileUpload.uploadFile(data, this);
                break;

            case Defaults.CHANGE_AVATAR:

                String url = fileUpload.uploadFile(data, this);
                fileUpload.uploadFile(data, this);
                BackendlessUser user = Backendless.UserService.CurrentUser();
                ((BackendlessUser) user).setProperty("avatar", url);
                //TODO
                Backendless.UserService.update(user, new AsyncCallback<BackendlessUser>() {
                    public void handleResponse(BackendlessUser user) {
                        // user has been updated
                    }

                    public void handleFault(BackendlessFault fault) {
                        // user update failed, to get the error code call fault.getCode()
                    }
                });
                break;


            case Defaults.URL_REQUEST:
                urlField.setText((String) data.getExtras().get(Defaults.DATA_TAG));
                takePhotoButton.setText(getResources().getText(R.string.takeAnotherPhoto));
        }

    }

    public static void showAlert(final Activity context, String message) {
        new AlertDialog.Builder(context).setTitle("An error occurred").setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                context.finish();
            }
        }).show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermissionRequestor.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // do your stuff
                } else {
                    Toast.makeText(this, "GET_ACCOUNTS Denied",
                            Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions,
                        grantResults);
        }
    }


    private void setAvatar(String imUrl) {
        Bitmap bitmap = null;
        try {
            URL url = new URL(imUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            bitmap = BitmapFactory.decodeStream(input);
        } catch (Exception e) {
        }
        avatarImage.setImageBitmap(bitmap);
    }
}

