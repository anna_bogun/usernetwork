package com.examples.usernetwork.file;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.async.callback.BackendlessCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.files.BackendlessFile;

import java.io.File;

public class FileUpload {

    public String uploadFile(Intent data, final Activity activity) {
        Uri currFileURI = data.getData();
        String path = currFileURI.getPath();

        Uri uri = data.getData();

        File file = null;

        file = new File(FileUtils.getPath(activity, uri));

        final ImageEntity[] resFile = {null};

        Backendless.Files.upload(file, Defaults.DEFAULT_PATH_ROOT + "/" + Backendless.UserService.CurrentUser().getEmail(), new AsyncCallback<BackendlessFile>() {
            @Override
            public void handleResponse(final BackendlessFile backendlessFile) {
                ImageEntity entity = new ImageEntity(System.currentTimeMillis(), backendlessFile.getFileURL());
                Backendless.Persistence.save(entity, new BackendlessCallback<ImageEntity>() {
                    @Override
                    public void handleResponse(ImageEntity imageEntity) {
                        Intent data = new Intent();
                        data.putExtra(Defaults.DATA_TAG, imageEntity.getUrl());
                        activity.setResult(Activity.RESULT_OK, data);
                    }
                });

                Toast.makeText(activity, "File was uploaded!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                Toast.makeText(activity, backendlessFault.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        return file.getPath();

    }
}

