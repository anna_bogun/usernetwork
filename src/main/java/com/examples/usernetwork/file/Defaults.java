package com.examples.usernetwork.file;

public class Defaults {

    public static final String APPLICATION_ID = "778920B3-E9E5-438D-FF8B-C6FD786FF200";
    public static final String API_KEY = "A2FE5753-4AEA-0400-FF5C-ABA6ADC8C900";
    public static final String SERVER_URL = "https://api.backendless.com";

    public static final int CAMERA_REQUEST = 101;
    public static final int UPLOAD_FILE_REQUEST = 103;
    public static final int URL_REQUEST = 102;
    public static final String DATA_TAG = "data";
    public static final String DEFAULT_PATH_ROOT = "fileservice";
    public static final int CHANGE_AVATAR = 105;
}
